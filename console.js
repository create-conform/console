/////////////////////////////////////////////////////////////////////////////////////////////
//
// console
//
//    Library for standardizing console logging.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error = require("error");
var event = require("event");
var clone = require("object-clone");

var _log = console.log;
var _error = console.error;
var _warning = console.warning;
var _info = console.info;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Function Overrides
//
/////////////////////////////////////////////////////////////////////////////////////////////
console.error = function(m){
    event.fire("error", arguments);
    if (m instanceof Error) {
        arguments[0] = clone(arguments[0]);
        arguments[0].name = arguments[0].name + ": " + arguments[0].message;
    }
    _error.apply(console,arguments);
};

console.log = function(m){
    event.fire("log", arguments);
    _log.apply(console,arguments);
};

console.warning = function(m){
    event.fire("warning", arguments);
    _warning.apply(console,arguments);
};

console.info = function(m){
    event.fire("info", arguments);
    _info.apply(console,arguments);
};

console.events = new event.Emitter(console);

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = console;